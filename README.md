## GJP-chrome-extension-tools

***


## 功能截图
### 首页
![](/png/home.png)
### 生成二维码
![](/png/qrcode.png)
### 生成身份证号码
![](/png/idcard.png)
### 模拟http请求
![](/png/httprequest.png)
### 右键菜单生成随机身份证号码
![](/png/right-menu-idcard.gif)
